<?php
/**
 * hook_form
 *
 * @return 
 *   An array containing the form elements to be displayed in the config form.
 */

function delicious_taxonomy_config_form(&$form_state) {
  $form = array();

  $form['configuration'] = array(
    '#type' => 'fieldset',
    '#title' => t('Delicious Taxonomy Configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );

  $form['configuration']['allow_user_vocab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow user vocabularies'),
    '#description' => t('If checked this allows users with appropriate privileges to create a vocabulary that carries their user name.  Their tags will be added to that vocabulary. <em>Note that this will cause a bloat if enabled for a large number of users</em>.  Each user that creates a vocabulary will have that vocabulary attached to the edit form of the nodes.  This potenial leads to bloat in the note edit forms'),
    '#default_value' => variable_get('delicious_taxonomy_allow_user_vocab', 0) ? 1:0,
  );

  $vocabs = array();
  $_vocabs = taxonomy_get_vocabularies();
  foreach ($_vocabs as $vocab) {
    $vocabs[$vocab->vid] = $vocab->name;
  }

  $form['configuration']['allow_system_vocab'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow system vocabulary'),
    '#description' => t('If checked this allows users with appropriate privileges to create terms from a system vocabulary.'),
    '#default_value' => variable_get('delicious_taxonomy_allow_system_vocab', 0) ? 1:0,
  );

  $form['configuration']['vocab'] = array(
    '#type' => 'select',
    '#title' => t('Vocabulary'),
    '#description' => t('Which vocabulary should imported tags be added to.'),
    '#default_value' => variable_get('delicious_taxonomy_vocab', ''),
    '#options' => $vocabs,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t('Save settings'));

  return $form;
}

function delicious_taxonomy_config_form_submit($form, &$form_state) {
  variable_set(
    'delicious_taxonomy_allow_user_vocab',
    $form_state['values']['allow_user_vocab']
  );

  variable_set(
    'delicious_taxonomy_allow_system_vocab',
    $form_state['values']['allow_system_vocab']
  );

  variable_set(
    'delicious_taxonomy_vocab',
    $form_state['values']['vocab']
  );
  drupal_set_message(t('Your config has been saved.'));
}
 /*
Array
(
    [5] => stdClass Object
        (
            [vid] => 5
            [name] => Tags
            [description] => 
            [help] => 
            [relations] => 1
            [hierarchy] => 0
            [multiple] => 1
            [required] => 0
            [tags] => 1
            [module] => taxonomy
            [weight] => 0
            [nodes] => Array
                (
                    [story] => story
                    [page] => page
                    [blog] => blog
                )

        )

    [2] => stdClass Object
        (
            [vid] => 2
            [name] => Toby Notes
            [description] => 
            [help] => 
            [relations] => 1
            [hierarchy] => 0
            [multiple] => 1
            [required] => 0
            [tags] => 1
            [module] => taxonomy
            [weight] => 0
            [nodes] => Array
                (
                    [toby_note] => toby_note
                )

        )

)
*/
